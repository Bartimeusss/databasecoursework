use confectionery;

select * from own_product;

select * from employee;

select * from work_type;

select e.id, e.first_name, e.last_name, e.patronymic, w.name as work_type_name
from employee e
join employee_work_type ew on e.id = ew.employee_id
join work_type w on w.id = ew.work_type_id;

select * from provider;

select * from deliver_product_type;

select p.id, p.name, p.country, p.price, pt.name as product_type_name
from deliver_product p 
join deliver_product_type pt on pt.id = p.product_type_id;

select d.id, d.delivery_date_time, d.product_count, p.price as price_per_one, p.price * d.product_count as summary_price, p.name as product_name, p.country as product_country, pt.name as product_type_name, pr.name as provider_name
from delivery d 
join provider pr on d.provider_id = pr.id
join deliver_product p on d.product_id = p.id
join deliver_product_type pt on p.product_type_id = pt.id;