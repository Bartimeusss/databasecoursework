create database confectionery;

use confectionery;

create table own_product
(
	id		int auto_increment primary key, 
    name	nvarchar(50),
	price	double
);

create table employee
(
	id			int auto_increment primary key, 
	first_name 	nvarchar(50),
    last_name  	nvarchar(50),
    patronymic 	nvarchar(50),
    born_year  	int,
    post		nvarchar(30),
	salary		double
);

create table work_type
(
	id		int auto_increment primary key, 
    name	nvarchar(100)
);

create table employee_work_type
(
	id				int auto_increment primary key, 
    employee_id		int,
    work_type_id 	int,
    foreign key(employee_id) references employee(id),
    foreign key(work_type_id) references work_type(id)
);

create table deliver_product_type
(
	id		int auto_increment primary key, 
    name 	nvarchar(50)
);

create table deliver_product
(
	id				int auto_increment primary key, 
    name			nvarchar(50),
    country			nvarchar(20),
    price			double,
    product_type_id	int,
    foreign key(product_type_id) references deliver_product_type(id)
);

create table provider
(
	id		int auto_increment primary key, 
    name	nvarchar(50),
    address	nvarchar(100)
);

create table delivery
(
	id					int auto_increment primary key, 
    delivery_date_time	datetime,
    product_count		int,
    product_id			int,
    provider_id			int,
    foreign key(product_id) references deliver_product(id),
    foreign key(provider_id) references provider(id)
);