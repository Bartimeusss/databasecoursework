-- dreamhome-data.sql

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE dreamhome;
--
-- Dumping data for table branch
--
SET AUTOCOMMIT=0;
INSERT INTO branch VALUES 
(1,'22 Deer Rd','London','SW1 4EH'),
(2,'16 Argyll St','Aberdeen','AB2 3SU'),
(3,'163 Main St','Glasgow','G11 9QX'),
(4,'32 Manse Rd','Bristol','BS99 1NZ'),
(5,'56 Clover Dr','London','NW10 6EU');
COMMIT;
--
-- Dumping data for table staff
--
INSERT INTO staff VALUES 
(1,'John','White','Manager','M','1995-10-01',3000.00,1),
(2,'Ann','Beech','Assistant','F','2000-11-10',1200.00,3),
(3,'David','Ford','Supervisor','M','1988-03-24',1800.00,2),
(4,'Mary','Howe','Assistant','F','1995-10-01',900.00,2),
(5,'Susan','Brand','Manager','F','1995-10-01',2400.00,3),
(6,'Jullie','Lee','Assistant','F','1995-10-01',900.00,1);
COMMIT;
--
-- Dumping data for table owner
--
INSERT INTO owner VALUES 
(1,'Joe','Keogh', '2 Fergus Dr, Aberdeen AB2n7SX','678912345'),
(2,'Carol','Farrel', '6 Achray St, Glasgow G32 9DX','567891234'),
(3,'Tina','Murphy', '63 Well St, Glasgow G42','456789123'),
(4,'Tony','Shaw', '12 Park Pl, Glasgow G4 QR','345678912');
COMMIT;
--
-- Dumping data for table property_for_rent
--
INSERT INTO property_for_rent VALUES 
(1,'16 Holhead','Aberdeen','AB7 5SU','House',6,650.00,1,4,2),
(2,'6 Argill St','London','NW2','Flat',4,400.00,2,6,1),
(3,'6 Lawrence St','Glasgow','G11 9QX','Flat',3,350.00,3,null,2),
(4,'2 Manor Rd','Glasgow','G32 4QX','Flat',3,375.00,4,2,3),
(5,'18 Dale Rd','Glasgow','G12','House',5,600.00,2,2,3),
(6,'5 Novar Dr','Glasgow','G12 9AX','Flat',4,450.00,4,3,3);
COMMIT;

--
-- Dumping data for table client
--
INSERT INTO client VALUES 
(1,'John','Kay','123456789','Flat',425.00),
(2,'Aline','Stewart','912345678','Flat',350.00),
(3,'Mike','Ritchie','891234567','House',750.00),
(4,'Mary','Tregear','789123456','Flat',600.00);
COMMIT;
--
-- Dumping data for table registration
--
INSERT INTO registration VALUES 
(1,1,6,'2015-01-02'),
(2,3,2,'2015-02-03'),
(3,3,2,'2015-03-04'),
(4,2,4,'2015-04-05');
COMMIT;

--
-- Dumping data for table viewing
--
INSERT INTO viewing VALUES 
(2,1,'2015-02-03','too small'),
(1,3,'2015-04-05','too remote'),
(2,3,'2015-05-06',NULL),
(4,1,'2015-06-07','no dining room'),
(2,4,'2015-07-08',NULL);
COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

